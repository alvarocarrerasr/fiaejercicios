/*familia([nombre(paco),edad(45),profesion(matematico)],
		[nombre(maria),edad(43),profesion(fisica)],
		[[nombre(miguel),edad(18),profesion(estudiante)],
		[nombre(ana),edad(17),profesion(estudiante)]]).

edad([_,edad(A),_],A).

edadMedia([],0):-!.
edadMedia([Cabeza|Cola],EMedia):-edad(Cabeza,E1),edadMedia(Cola,EC),EMedia is E1 + EC.*/


familia(persona(juan,perez,43),
		persona(maria,alonso,41),
		[persona(carlos,perez,19),
		persona(andres,perez,3),
		persona(elena,perez,100)]).
edad(persona(_,_,EDAD),EDAD).

calculaTAM([],0).
calculaTAM([Cabeza|Cola],Res):-calculaTAM(Cola,Tmp1),Res is Tmp1 + 1.
primerHijo([Cabeza|Cola],Cabeza).
colaLista([Cabeza|Cola],Cola).

edadMedia([],0).
edadMedia([Cabeza|Cola],EM):-edad(Cabeza,Tmp1),edadMedia(Cola,Tmp2),EM is Tmp1 + Tmp2.


calculaEdad([Cabeza|Cola],Resultado):-calculaTAM([Cabeza|Cola],Tmp2),edadMedia([Cabeza|Cola],Tmp0),Resultado is Tmp0/Tmp2.

dameMayorHijo([],Tmp0,Tmp0):-!.
dameMayorHijo([Cabeza|Cola],Tmp0,Tmp1):-edad(Cabeza,Tmp2),edad(Tmp0,Tmp3),Tmp2>Tmp3,dameMayorHijo(Cola,Cabeza,Tmp1).
dameMayorHijo([Cabeza|Cola],Tmp0,Tmp1):-edad(Cabeza,Tmp2),edad(Tmp0,Tmp3),Tmp3>=Tmp2,dameMayorHijo(Cola,Tmp0,Tmp1).


dameMayorHijos([Padre,Madre,Hijos],Resultado):-primerHijo(Hijos,Resu),colaLista(Hijos,Cola),dameMayorHijo(Cola,Resu,Resultado),!.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



enlace(valladolid,palencia,90).
enlace(valladolid,segovia,100).
enlace(valladolid,burgos,120).
enlace(valladolid,zamora,100).
enlace(segovia,madrid,87).
enlace(madrid,badajoz,103).
enlace(madrid,guadalajara,70).
enlace(madrid,toledo,67).
enlace(madrid,sevilla,250).
enlace(madrid,barcelona,400).
enlace(barcelona,castellon,300).
enlace(castellon,valencia,100).
enlace(valencia,denia,70).
enlace(denia,mallorca,100).
enlace(mallorca,ibiza,50).
enlace(ibiza,cabrera,20).
enlace(cabrera,italia,500).

como_ir(X,Y):-enlace(X,Y,_).
como_ir(X,Y):-enlace(X,Z,_),como_ir(Z,Y).

cuantos_pasos(X,Y,1):-enlace(X,Y,_).
cuantos_pasos(X,Y,SOL):-enlace(X,Z,_),cuantos_pasos(Z,Y,SOL0),SOL is SOL0+1.


distancia_entre_ciudades(X,Y,DT):-enlace(X,Y,DT).
distancia_entre_ciudades(X,Y,DT):-enlace(X,Z,D0),distancia_entre_ciudades(Z,Y,D1),DT is D0 + D1,!.
