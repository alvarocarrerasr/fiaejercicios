familia(persona(juan,perez,43),
		persona(maria,alonso,41),
		[persona(carlos,perez,19),
		persona(andres,perez,3),
		persona(elena,perez,100)]).
edad(persona(_,_,EDAD),EDAD):-!.

cuantosSon([],0).
cuantosSon([Cabeza|Cola],Lenght):-cuantosSon(Cola,Lenght1),Lenght is Lenght1+1.

/*DAME EL MAYOR*/


dameMayor([MAYOR],MAYOR):-!.
dameMayor([A,B|Cola],Resultado):-edad(A,Tmp0),edad(B,Tmp1),Tmp0>=Tmp1,dameMayor([A|Cola],Resultado).
dameMayor([A,B|Cola],Resultado):-edad(A,Tmp0),edad(B,Tmp1),Tmp1>Tmp0,dameMayor([B|Cola],Resultado).

/*Dame media aritmética*/
dameAritmetica([],0).
dameAritmetica([Cabeza|Cola],Resultado):-edad(Cabeza,Tmp0),dameAritmetica(Cola,Resultado0),Resultado is Resultado0+Tmp0.

getMedia(Familia,Resultado):-dameAritmetica(Familia,Resultado0),cuantosSon(Familia,Len),Resultado is Resultado0/Len.
