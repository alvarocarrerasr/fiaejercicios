suma([],0):-!.
suma([Cabeza|Cola],Resultado):-suma(Cola,Resultado2), Resultado is Cabeza+Resultado2.


calculaTAM([],0):-!.
calculaTAM([Cabeza|Cola],Resultado):-calculaTAM(Cola,Resultado2), Resultado is 1+Resultado2.

sublista(X,[_],[]):-!.
sublista(X,[X|Cola],[]).
sublista(X,[Cabeza|Cola],[Cabeza|Res]):-sublista(X,Cola,Res).



eliminaPosicion([Cabeza|Cola],1,Cola):-!.
eliminaPosicion([Cabeza|Cola],Ite,[Cabeza|Res]):-Parc1 is Ite-1,eliminaPosicion(Cola,Parc1, Res).

eliminaEnesima(Lista, Resultado):-eliminaPosicion(Lista, 9, Resultado).

maximo([MAXIMO],MAXIMO).
maximo([A,B|Lista],Resultado):-A>=B,maximo([A|Lista],Resultado),!.
maximo([A,B|Lista],Resultado):-A<B,maximo([B|Lista],Resultado),!.

familia(persona(juan,perez,5000),
		persona(maria,alonso,10000),
		[persona(carlos,perez,0),
		persona(andres,perez,0),
		persona(elena,perez,200)]).
familia(persona(pedro,lopez,25000),
		persona(carmen,ruiz,15000),
		[persona(carlos,lopez,1000),
		persona(teresa,lopez,0)]).
sueldo(persona(_,_,S),S).

total([],0):-!.
total([Cabeza|Cola],Subtotal1):-sueldo(Cabeza,S),total(Cola,Subtotal0), Subtotal1 is Subtotal0+S.




pasaBinario(0,ResultadosIntermedios,ResultadosIntermedios):-!.
pasaBinario(Num,ResultadosIntermedios,Res):-Tmp1 is Num mod 2, Tmp2 is Num // 2, pasaBinario(Tmp2, [Tmp1|ResultadosIntermedios],Res).

binario(Num,Resul):-pasaBinario(Num,[],Resul).
