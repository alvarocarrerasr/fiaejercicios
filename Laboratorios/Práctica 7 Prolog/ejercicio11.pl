libro_de_familia(esposo(nombre(antonio,garcia,fernandez), profesion(arquitecto), salario(30000)),              
	esposa(nombre(ana,ruiz,lopez), profesion(docente), salario(12000)),  domicilio(sevilla)). 


libro_de_familia(esposo(nombre(luis,alvarez,garcia),profesion(arquitecto), salario(40000)),  
	esposa(nombre(ana,romero,soler), profesion(sus_labores), salario(0)),  domicilio(sevilla)).

libro_de_familia(esposo(nombre(bernardo,bueno,martinez), profesion(docente), salario(12000)),  
	esposa(nombre(laura,rodriguez,millan), profesion(medico), salario(25000)),  domicilio(cuenca)).

libro_de_familia(esposo(nombre(miguel,gonzalez,ruiz), profesion(empresario), salario(40000)),
	esposa(nombre(belen,salguero,cuevas), profesion(sus_labores), salario(0)),  domicilio(dos_hermanas)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

que_profesion(X,Y,Z,S):-libro_de_familia(esposo(nombre(X,Y,Z), profesion(S), salario(_)),esposa(nombre(_,_,_), profesion(_), salario(_)),  domicilio(_)).

que_profesion(X,Y,Z,S):-libro_de_familia(esposo(nombre(_,_,_), profesion(_), salario(_)),esposa(nombre(X,Y,Z), profesion(S), salario(_)),  domicilio(_)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ingresos_totales(CIUDAD,ING):-libro_de_familia(esposo(nombre(_,_,_), profesion(_), salario(A)),esposa(nombre(_,_,_), profesion(_), salario(B)),  domicilio(CIUDAD)), ING is A+B.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mas_que(CANTIDAD,C):-libro_de_familia(esposo(nombre(_,_,_), profesion(_), salario(A)),esposa(nombre(_,_,_), profesion(_), salario(B)),  domicilio(C)), SAL is A+B,SAL>CANTIDAD.
menos_que(CANTIDAD,C):-libro_de_familia(esposo(nombre(_,_,_), profesion(_), salario(A)),esposa(nombre(_,_,_), profesion(_), salario(B)),  domicilio(C)), SAL is A+B,SAL<CANTIDAD.