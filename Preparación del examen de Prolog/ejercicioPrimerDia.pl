enlace(pprincipal,formacion).
enlace(pprincipal,isistemas).
enlace(formacion,creatividad).
enlace(formacion,innovacion).
enlace(isistemas,calidad).
enlace(isistemas,mabiente).
enlace(creatividad,quees).
enlace(creatividad,guia).
enlace(calidad,iso).
enlace(guia,queesidea).
enlace(guia,comotenerideas).
enlace(iso,sistgest).
enlace(iso,recursos).

mideLongLista([],0):-!.
mideLongLista([Cabeza|Cola],Tam):-mideLongLista(Cola,P),Tam is P+1.

cuelga_de(X,Y):-enlace(X,Y).
cuelga_de(X,Y):-enlace(X,Z),cuelga_de(Z,Y),!.

como_ir(Y,L,[L|Final]):-enlace(L,Y),!.
como_ir(X,L,[X|Resultado]):-enlace(Y,X),como_ir(Y,L,Resultado).

nrClicks(X,Y,Tam):-como_ir(X,Y,Resultado),mideLongLista(Resultado,Tam).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

libro_de_familia(esposo(nombre(antonio,garcia,fernandez), profesion(arquitecto), salario(30000)),
				esposa(nombre(ana,ruiz,lopez), profesion(docente), salario(12000)),
				domicilio(sevilla)).
libro_de_familia(esposo(nombre(luis,alvarez,garcia), profesion(arquitecto), salario(40000)),
				esposa(nombre(ana,romero,soler), profesion(sus_labores), salario(0)),
				domicilio(sevilla)).
libro_de_familia(esposo(nombre(bernardo,bueno,martinez), profesion(docente), salario(12000)),
				esposa(nombre(laura,rodriguez,millan), profesion(medico), salario(25000)),
				domicilio(cuenca)).
libro_de_familia(esposo(nombre(miguel,gonzalez,ruiz), profesion(empresario), salario(40000)),
				esposa(nombre(belen,salguero,cuevas), profesion(sus_labores), salario(0)),
				domicilio(dos_hermanas)).


quien_es(Nombre,Profesion):-libro_de_familia(esposo(Nombre,Profesion,_),_,_).
quien_es(Nombre,Profesion):-libro_de_familia(_,esposa(Nombre,Profesion,_),_).

salario(esposo(_,_,salario(S0)),esposa(_,_,salario(S1)),ST):-ST is S0+S1.

%%ConsultaSalario:    libro_de_familia(Esposo,Esposa,_),salario(Esposo,Esposa,S).

ganaMas(X,S):-libro_de_familia(esposo(X,_,salario(X0)),_,_),X0>=S.
ganaMas(X,S):-libro_de_familia(_,esposa(X,_,salario(X0)),_),X0>=S.


ganaMenos(X,S):-libro_de_familia(esposo(X,_,salario(X0)),_,_),X0=<S.
ganaMenos(X,S):-libro_de_familia(_,esposa(X,_,salario(X0)),_),X0=<S.

