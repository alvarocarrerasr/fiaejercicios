=============================================
= 				EJERCICIO 3					=
=============================================


a) Dará fallo, porque estamos haciendo una asignación a constantes.
b) Se obtendrá False porque no coinciden las constantes, además que que no estamos obligando a prolog a evaluar.
c) Dará true porque evaluará las constantes de la izquierda y las de la derecha, el resultado es el mismo, por lo que continuará,
d)  Dará true porque los símbolos no coinciden

=============================================
= 				EJERCICIO 4					=
=============================================

Primero comenzaria evaluando la expresión, 
q(X):-p(X), por lo que le llevaría a la segunda
p(2):-0 is 2 mod 2, asignando a la constante 0
el valor 0.
A continuación seguiría evaluando, sacando que X1
es 2+1 (3), X1 es 3 y calcularía el valor de p(X1).
Pasaría finalmente por p(X1) y vería que 0, que al
ser constante tiene un valor definido y no mutable
no es igual al resultado de 3 módulo 2, por lo que 
terminaria con false.
