Los operadoras =< y >= no funcionan igual q en otros lenguajes.


4 ?- comida(consome,X,Y).
X = filete_de_cerdo,
Y = flan ;
X = filete_de_cerdo,
Y = nueces_con_miel ;
X = filete_de_cerdo,
Y = naranja ;
X = pollo_asado,
Y = flan ;
X = pollo_asado,
Y = nueces_con_miel ;
X = pollo_asado,
Y = naranja ;
X = trucha,
Y = flan ;
X = trucha,
Y = nueces_con_miel ;
X = trucha,
Y = naranja ;
X = bacalao,
Y = flan ;
X = bacalao,
Y = nueces_con_miel ;
X = bacalao,
Y = naranja.



5 ?- comida_equilibrada(X,Y,naranja).
X = paella,
Y = pollo_asado ;
X = paella,
Y = trucha ;
X = paella,
Y = bacalao ;
X = gazpacho,
Y = pollo_asado ;
X = gazpacho,
Y = trucha ;
X = gazpacho,
Y = bacalao ;
X = consome,
Y = trucha ;
false.

?7 ?- valor(X,Y,Z,500).